# howtogit

## This is how you git

get the repo
```
git clone https://gitlab.com/KingJacker/howtogit.git
```

add changes
```
git add .
```

add changes on some files
```
git add -p
```

commit changes

```
git commit -m "commit message"
```

check status

```
git status
```

### Branches
check branches

```
git branch
```

create new branch

```
git branch newBranch
```

switch branch
```
git checkout newBranch
```

merge branches
```
git merge newBranch # while in branch you want to merge to
```